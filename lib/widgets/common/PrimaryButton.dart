import 'package:flutter/material.dart';

import './LoadingBrightWidget.dart';

class PrimaryButton extends StatelessWidget {

  final void Function()? onPressed;
  final String text;
  final bool? isProcessing;

  PrimaryButton({required this.onPressed, required this.text, this.isProcessing});

  @override
  Widget build(BuildContext context) {
    Widget buttonText = Text(text, style: Theme.of(context).textTheme.subtitle1!.copyWith(color: Colors.white));
    if (isProcessing != null && isProcessing!) {
      buttonText = Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          LoadingBrightWidget(),
          SizedBox(width: 10,),
          buttonText
        ],
      );

    }

    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        primary: Color(0XFF1E375A),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0),
        ),

        padding: EdgeInsets.only(top: 13, bottom: 13),
      ),
      onPressed: onPressed,
      child: buttonText
    );
  }
}