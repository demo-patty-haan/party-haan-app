import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class LoadingBrightWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Theme(
        data: ThemeData(
          cupertinoOverrideTheme: CupertinoThemeData(brightness: Brightness.dark)
        ),
        child: CupertinoActivityIndicator()
      ),
    );
  }
}
