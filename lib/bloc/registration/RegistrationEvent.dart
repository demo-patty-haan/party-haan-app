import 'package:equatable/equatable.dart';

abstract class RegistrationEvent extends Equatable {
  RegistrationEvent();

  @override
  List<Object> get props => [];
}

class SubmitUserAndPassword extends RegistrationEvent {
  final String username;
  final String password;

  SubmitUserAndPassword({required this.username, required this.password});
}
