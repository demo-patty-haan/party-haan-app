import 'package:equatable/equatable.dart';

import '../../models/Party.dart';

abstract class PartyBoxState extends Equatable {
  PartyBoxState();

  @override
  List<Object> get props => [];
}

class PartyBoxInitial extends PartyBoxState {}

class PartyBoxProcessing extends PartyBoxState {}

class PartyBoxLoaded extends PartyBoxState {
  final Party party;
  final String? message;

  PartyBoxLoaded({required this.party, this.message});
}

class PartyBoxFailure extends PartyBoxState {
  final String message;

  PartyBoxFailure({required this.message});
}