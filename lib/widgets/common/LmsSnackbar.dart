import 'package:flutter/material.dart';

class LmsSnackbar{

  static final LmsSnackbar _singleton = LmsSnackbar._internal();
  factory LmsSnackbar() {
    return _singleton;
  }
  LmsSnackbar._internal();

  SnackBar error({required BuildContext context, required String message, Key? contentKey}) {
    return base(context: context, message: message, color: Color(0xffd13914), contentKey: contentKey);
  }

  SnackBar success({required BuildContext context, required String message, Key? contentKey}) {
    return base(context: context, message: message, color: Color(0xff1b995f), contentKey: contentKey);
  }

  SnackBar base({required BuildContext context, required String message, required Color color, Key? contentKey}) {
    Widget content;
    if (contentKey == null) {
      content =  Text(message);
    } else {
      content =  Text(message, key: contentKey,);
    }

    return SnackBar(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(7.0),
          topRight: Radius.circular(7.0)
        ),
      ),
      content: content,
      backgroundColor: color
    );
  }
}