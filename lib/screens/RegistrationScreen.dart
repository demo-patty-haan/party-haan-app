import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../bloc/registration/Registration.dart';
import '../bloc/authenticate/AuthenticationBloc.dart';
import '../managers/AuthenticationManager.dart';
import '../repositories/UserRepository.dart';

import '../widgets/common/DefaultTextInput.dart';
import '../widgets/common/PasswordTextField.dart';
import '../widgets/common/PrimaryButton.dart';

class RegistrationScreen extends StatefulWidget {
  RegistrationScreen({Key? key}) : super(key: key);

  @override
  _RegistrationState createState() => _RegistrationState();
}

class _RegistrationState extends State<RegistrationScreen> {
  late TextEditingController emailController;
  late TextEditingController passwordController;
  late TextEditingController confirmPasswordController;
  final _formKey = GlobalKey<FormState>();

  bool isConfirm = false;

  @override
  void initState() {
    super.initState();

    emailController = TextEditingController();
    passwordController = TextEditingController();
    confirmPasswordController = TextEditingController();
  }

  @override
  void dispose() {
    emailController.dispose();
    passwordController.dispose();
    confirmPasswordController.dispose();

    super.dispose();
  }

  void toggleConfirm() {
    setState(() {
      isConfirm = !isConfirm;
    });
  }

  void onFormSubmit(BuildContext context) {
    if (_formKey.currentState!.validate()) {
      BlocProvider.of<RegistrationBloc>(context).add(SubmitUserAndPassword(
        username: emailController.text,
        password: passwordController.text
      ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<RegistrationBloc>(
      create: (BuildContext context) {
        return RegistrationBloc(
          userRepository: context.read<UserRepository>(),
          authenticationManager: context.read<AuthenticationManager>(),
          authenticationBloc: context.read<AuthenticationBloc>()
        );
      },
      child: BlocConsumer<RegistrationBloc, RegistrationState>(
        listener: _mainContentListenter,
        builder: _mainContentBuilder,
      ),
    );
  }

  void _mainContentListenter(BuildContext context, RegistrationState state) async {
    if (state is RegistrationSuccessfull) {
      Navigator.pop(context, state.user);
    }
  }

  Widget _mainContentBuilder(BuildContext context, RegistrationState state) {
    return  Scaffold(
      body: Container(
        width: 1.sw,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.bottomLeft,
            end: Alignment.topRight,
            stops: [0.25, 0.75],
            colors: [
              Color.fromRGBO(225,118,173,1.0),
              Color.fromRGBO(110,198,255,1.0),
            ],
          )
        ),
        alignment: Alignment.center,
        child: SingleChildScrollView(
          padding: EdgeInsets.only(left: 24.w, right: 24.w),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Center(
                  child: Text('สร้างบัญชีผู้ใช้', style: Theme.of(context).textTheme.headline5,),
                ),
                SizedBox(height: 32,),
                Text('อีเมล', style: Theme.of(context).textTheme.subtitle1,),
                SizedBox(height: 8,),
                DefaultTextInput(
                  label:  'อีเมล',
                  validator: (value) {
                    if (value == null) {
                      return null;
                    }
                    if (value.isEmpty) {
                      return 'เกิดข้อผิดพลาด';
                    }
                    return null;
                  },
                  textEditingController: emailController,
                ),
                SizedBox(height: 16,),
                Text('รหัสผ่าน', style: Theme.of(context).textTheme.subtitle1,),
                SizedBox(height: 8,),
                PasswordTextField(
                  textEditingController: passwordController,
                  label: 'รหัสผ่าน',
                  validator: (value) {
                    if (value == null) {
                      return null;
                    }
                    if (value.isEmpty) {
                      return 'เกิดข้อผิดพลาด';
                    }
                    if (value != confirmPasswordController.text) {
                      return 'รหัสไม่ตรงกัน';
                    }
                    if (value.length < 6) {
                      return 'รหัสอย่างน้อย 6 ตัวอักษร';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 16,),
                Text('ยืนยันรหัสผ่าน', style: Theme.of(context).textTheme.subtitle1,),
                SizedBox(height: 8,),
                PasswordTextField(
                  textEditingController: confirmPasswordController,
                  label: 'ยืนยันรหัสผ่าน',
                  validator: (value) {
                    if (value == null) {
                      return null;
                    }
                    if (value.isEmpty) {
                      return 'เกิดข้อผิดพลาด';
                    }
                    if (value != passwordController.text) {
                      return 'รหัสไม่ตรงกัน';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 24,),
                Row(
                  children: <Widget>[
                    Checkbox(
                      value: isConfirm,
                      onChanged: (val) {
                        toggleConfirm();
                      },
                    ),
                    Expanded(
                      child: InkWell(
                        onTap: toggleConfirm,
                        child: Text('ฉันยอมรับเงื่อนไขและข้อตกลงเกี่ยวกับการใช้งาน', style: Theme.of(context).textTheme.subtitle1),
                      )
                    )
                  ],
                ),
                SizedBox(height: 16,),
                Center(
                  child: SizedBox(
                    width: 0.5.sw,
                    child: PrimaryButton(
                      onPressed: (state is! RegistrationProcessing && isConfirm) ? () {
                        onFormSubmit(context);
                      } : null,
                      text: 'ยืนยัน',
                      isProcessing: (state is RegistrationProcessing),
                    ),
                  ),
                ),
                SizedBox(height: 16,),
                Center(
                  child: InkWell(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Text('มีบัญชีผู้ใช้แล้ว?', style: Theme.of(context).textTheme.subtitle1!.copyWith(decoration: TextDecoration.underline)),
                  )
                )
              ]
            )
          )
        )
      )
    );
  }
}