import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../bloc/authenticate/AuthenticationBloc.dart';
import '../../bloc/partyBox/PartyBox.dart';
import '../../managers/PartyManager.dart';
import '../../models/Party.dart';

import '../../widgets/alert/ConfirmAlertDialog.dart';
import '../../widgets/common/LmsSnackbar.dart';

class PartyBox extends StatefulWidget {
  final List<Party> parties;
  final int index;

  PartyBox({Key? key, required this.parties, required this.index}) : super(key: key);

  @override
  State<PartyBox> createState() => _PartyBoxState();
}

class _PartyBoxState extends State<PartyBox> {

  @override
  void initState() {

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<PartyBoxBloc>(
      create: (BuildContext context) {
        return PartyBoxBloc(
          authenticationBloc: context.read<AuthenticationBloc>(),
          partyManager: context.read<PartyManager>()
        );
      },
      child: BlocConsumer<PartyBoxBloc, PartyBoxState>(
        listener: _mainContentListenter,
        builder: _mainContentBuilder
      )
    );
  }

  void _mainContentListenter(BuildContext context, PartyBoxState state) async {
    if (state is PartyBoxLoaded) {
      widget.parties[widget.index] = state.party;
      if (state.message != null) {
        ScaffoldMessenger.of(context)..hideCurrentSnackBar()..showSnackBar(
          LmsSnackbar().success(context: context, message: state.message!)
        );
      }

    }
  }

  Widget _mainContentBuilder(BuildContext context, PartyBoxState state) {
    Widget button;

    if (widget.parties[widget.index].isFull) {
      button = ElevatedButton(
        style: ElevatedButton.styleFrom(
          elevation: 0,
        ),
        onPressed: null,
        child: Text('เต็ม')
      );
    } else if (widget.parties[widget.index].isJoined! == false) {
      button = ElevatedButton(
        style: ElevatedButton.styleFrom(
          elevation: 0,
        ),
        onPressed: (state is! PartyBoxProcessing) ? () async {
          final response2 = await showDialog<String>(
            context: context,
            barrierDismissible: false, // user must tap button!
            builder: (BuildContext context) {
              return ConfirmationAlertDialog(
                questionMessage: 'เข้าร่วมปาร์ตี้',
                confirmMessage: 'กรุณายืนยันเ',
              );
            },
          );
          if (response2 == ConfirmationAlertDialog.confirm) {
            context.read<PartyBoxBloc>().add(JoinParty(party: widget.parties[widget.index]));
          }
        } : null,
        child: Text('เข้าร่วม')
      );
    } else {
      button = ElevatedButton(
        style: ElevatedButton.styleFrom(
          elevation: 0,
          primary: Colors.green[300]
        ),
        onPressed: (state is! PartyBoxProcessing) ? () async {
          final response2 = await showDialog<String>(
            context: context,
            barrierDismissible: false, // user must tap button!
            builder: (BuildContext context) {
              return ConfirmationAlertDialog(
                questionMessage: 'ยกเลิกการเข้าร่วมปาร์ตี้',
                confirmMessage: 'กรุณายืนยันเ',
              );
            },
          );
          if (response2 == ConfirmationAlertDialog.confirm) {
            context.read<PartyBoxBloc>().add(UnJoinParty(party: widget.parties[widget.index]));
          }
        } : null,
        child: Text('ยกเลิก')
      );
    }

    return Column(
      children: [
        Container(
          height: 150,
          decoration: BoxDecoration(
            color: Colors.pink[100]
          ),
          child: Center(
            child: Icon(Icons.image_outlined, size: 100,),
          ),
        ),
        Container(
          width: double.infinity,
          decoration: BoxDecoration(
            color: Colors.pink[50]
          ),
          padding: EdgeInsets.all(8.w),
          child: IntrinsicWidth(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Container(
                  height: 40,
                  child: Text(widget.parties[widget.index].name ?? '', maxLines: 2, overflow: TextOverflow.ellipsis, style: Theme.of(context).textTheme.bodyText2,),
                ),
                SizedBox(height: 8,),
                Container(
                  height: 1,
                  color: Colors.black26,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('${widget.parties[widget.index].totalUsers}/${widget.parties[widget.index].maxTotalUsers}', style: Theme.of(context).textTheme.bodyText2),
                    button
                  ],
                )
              ],
            ),
          )
        ),
      ],
    );
  }
}