import 'dart:convert';
import 'dart:io';
import 'package:dio/dio.dart';

import '../data/ApiClient.dart';
import '../models/User.dart';

class UserRepository {
  final ApiClient apiClient;

  UserRepository({required this.apiClient});

  Map<String, dynamic> decodeResponse(Response response) {
    if (response.data is String) {
      return jsonDecode(response.data);
    }
    return response.data;
  }

  Future<User> getCurrentUser() async {
    final String path = '/auth/v1/user/me';

    Response response = await apiClient.get(path);

    if (response.statusCode != HttpStatus.ok) {
      throw('session invalid');
    }

    Map<String, dynamic> json = decodeResponse(response);
     return User.fromJson(json['user']);
  }

  Future<User> createUser({required String username, required String password}) async {
    final String path = '/public/v1/user';

    Map<String, dynamic> data = {
      'username': username,
      'password': password,
    };
    Response response = await apiClient.post(path, data, auth: false);
    Map<String, dynamic> json = decodeResponse(response);

    if (response.statusCode! >= 400) {
      throw 'Something went wrong';
    }

    return User.fromJson(json['user']);
  }
}