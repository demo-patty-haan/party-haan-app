import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../bloc/authenticate/Authenticate.dart';
import './LoginMainScreen.dart';
import './LoadingScreen.dart';
import './MainScreen.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key? key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: Size(380, 670),
      builder: () => BlocConsumer<AuthenticationBloc, AuthenticationState>(
        listener: (BuildContext context, AuthenticationState state){

        },
        builder: (BuildContext context, AuthenticationState state) {
          if (state is AuthenticationUnauthenticated) {
            return LoginMainScreen();
          } else if (state is AuthenticationAuthenticated) {
            return MainScreen();
          }
          return LoadingScreen();
        }
      )
    );
  }
}

