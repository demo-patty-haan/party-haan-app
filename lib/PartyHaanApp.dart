import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import './data/ApiClient.dart';
import './bloc/authenticate/Authenticate.dart';
import './repositories/UserRepository.dart';
import './repositories/AuthenticationRepository.dart';
import './repositories/PartyRepository.dart';
import './repositories/PartyUserRepository.dart';
import './managers/AuthenticationManager.dart';
import './managers/PartyManager.dart';

import './screens/HomeScreen.dart';
import './screens/RegistrationScreen.dart';
import './screens/PartyFormScreen.dart';

class PartyHaanApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
      providers: [
        RepositoryProvider<FlutterSecureStorage>(
          create: (BuildContext context) {
            return FlutterSecureStorage();
          },
        ),
        RepositoryProvider<ApiClient>(
          create: (BuildContext context) {
            return ApiClient(
              storage: context.read<FlutterSecureStorage>(),
            );
          },
        ),
        RepositoryProvider<UserRepository>(
          create: (BuildContext context) {
            return UserRepository(
              apiClient: context.read<ApiClient>(),
            );
          }
        ),
        RepositoryProvider<AuthenticationRepository>(
          create: (BuildContext context) {
            return AuthenticationRepository(
              apiClient: context.read<ApiClient>(),
            );
          }
        ),
        RepositoryProvider<PartyRepository>(
          create: (BuildContext context) {
            return PartyRepository(
              apiClient: context.read<ApiClient>(),
            );
          }
        ),
        RepositoryProvider<PartyUserRepository>(
          create: (BuildContext context) {
            return PartyUserRepository(
              apiClient: context.read<ApiClient>(),
            );
          }
        ),
        RepositoryProvider<AuthenticationManager>(
          create: (BuildContext context) {
            return AuthenticationManager(
              authenticationRepository: context.read<AuthenticationRepository>(),
              userRepository:context.read<UserRepository>(),
              storage: context.read<FlutterSecureStorage>(),
            );
          }
        ),
        RepositoryProvider<PartyManager>(
          create: (BuildContext context) {
            return PartyManager(
              partyRepository: context.read<PartyRepository>(),
              partyUserRepository: context.read<PartyUserRepository>(),
            );
          }
        )
      ],
      child: BlocProvider<AuthenticationBloc>(
        create: (context) {
          return AuthenticationBloc(
            userRepository: context.read<UserRepository>(),
            authenticationManager: context.read<AuthenticationManager>(),
          )..add(AppStarted());
        },
        child: MaterialApp(
          title: 'Security App',
          initialRoute: '/',
          routes: {
            '/': (context) => HomeScreen(),
            'registration': (context) => RegistrationScreen(),
            'party-form': (context) => PartyFormScreen()
          },
          theme: ThemeData(
            primaryColor:  Color.fromRGBO(225,118,173,1.0)
          )
        ),
      )
    );
  }
}