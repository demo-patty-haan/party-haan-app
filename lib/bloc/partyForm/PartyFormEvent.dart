import 'package:equatable/equatable.dart';

abstract class PartyFormEvent extends Equatable {
  PartyFormEvent();

  @override
  List<Object> get props => [];
}

class PartyFormSubmit extends PartyFormEvent {
  final String name;
  final int maxTotalUser;

  PartyFormSubmit({required this.name, required this.maxTotalUser});
}