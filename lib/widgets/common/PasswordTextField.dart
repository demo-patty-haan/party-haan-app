import 'package:flutter/material.dart';

class PasswordTextField extends StatefulWidget{
  final TextEditingController? textEditingController;
  final String label;
  final String? Function(String?)? validator;
  final void Function(String?)? onSaved;
  PasswordTextField({Key? key, this.textEditingController, required this.label, required this.validator, this.onSaved}): super(key: key);

  @override
  State<PasswordTextField> createState() => _PasswordTextFieldState();
}

class _PasswordTextFieldState extends State<PasswordTextField> {
  //TextEditingController get _controller => widget.controller;
  bool hidePassword = true;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      obscureText: hidePassword,
      controller: widget.textEditingController,
      decoration: InputDecoration(
        filled: true,
        fillColor: Colors.white,
        labelText: widget.label,
        floatingLabelBehavior: FloatingLabelBehavior.never,
        alignLabelWithHint: true,
        border: InputBorder.none,
        contentPadding: const EdgeInsets.only(left: 14.0, bottom: 20.0, top: 10.0),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Color(0XFFCBCBCB)),
          borderRadius: BorderRadius.circular(10),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Color(0XFFCBCBCB)),
          borderRadius: BorderRadius.circular(10),
        ),
        errorBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Theme.of(context).errorColor),
          borderRadius: BorderRadius.circular(10),
        ),
        disabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Theme.of(context).hoverColor),
          borderRadius: BorderRadius.circular(10),
        ),
        focusedErrorBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Theme.of(context).errorColor),
          borderRadius: BorderRadius.circular(10),
        ),
        suffixIcon: IconButton(
          icon: Icon(Icons.visibility),
          onPressed: () {
            setState(() {
              hidePassword = !hidePassword;
            });
          },
        )
      ),
      validator: widget.validator,
      onSaved: widget.onSaved,
    );
  }
}