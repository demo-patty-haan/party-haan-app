import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'dart:async';

import '../models/Party.dart';
import '../repositories/PartyRepository.dart';
import '../bloc/partyListing/PartyListing.dart';

import '../widgets/common/LmsSnackbar.dart';
import '../widgets/party/PartyBox.dart';
import '../widgets/common/LoadingWidget.dart';

class PartyListingScreen extends StatefulWidget {
  PartyListingScreen({Key? key}) : super(key: key);

  @override
  State<PartyListingScreen> createState() => _PartyListingScreenState();
}

class _PartyListingScreenState extends State<PartyListingScreen> {
  List<Party>? _parties;
  Completer? _completer;

  late ScrollController _scrollController;
  late bool _hasReachMaxRecord;
  late bool _scrollLock;
  final _scrollThreshold = 33.0;

  @override
  void initState() {
    _scrollController = ScrollController();
    _hasReachMaxRecord = false;
    _scrollLock = false;

    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    return BlocProvider<PartyListingBloc>(
      create: (BuildContext context) {
        PartyListingBloc partyListingBloc = PartyListingBloc(
          partyRepository: context.read<PartyRepository>()
        )..add(PartyListFetch());

        _scrollController.addListener(() {
          final maxScroll = _scrollController.position.maxScrollExtent;
          final currentScroll = _scrollController.position.pixels;
          if ((_scrollLock == false) && (_hasReachMaxRecord == false) && (maxScroll - currentScroll <= _scrollThreshold)) {
            _scrollLock = true;
            partyListingBloc.add(PartyListFetchMore(parties: _parties!));
          }
        });

        return partyListingBloc;
      },
      child: BlocConsumer<PartyListingBloc, PartyListState>(
        listener: _mainContentListenter,
        builder: _mainContentBuilder
      )
    );
  }

  void _mainContentListenter(BuildContext context, PartyListState state) async {
    if (state is PartyListLoaded) {
      print('Party Loaded');
      _parties = state.parties;
      _hasReachMaxRecord = state.hasReachMax;
      _scrollLock = false;
      if (_completer != null && !_completer!.isCompleted) {
        _completer!.complete();
      }
    } else if (state is PartyListFailure) {
      ScaffoldMessenger.of(context)..hideCurrentSnackBar()..showSnackBar(
        LmsSnackbar().success(context: context, message: state.message)
      );
    }
  }

  Widget _mainContentBuilder(BuildContext context, PartyListState state) {
    return _body(context, state);
  }

  Widget _body(BuildContext context, PartyListState state) {
    Widget item;

    if ((state is PartyListProcessing || state is PartyListInitial) && _parties == null) {
      item = Center(
        child: LoadingWidget(),
      );
    } else {
      print(_parties!.length);
      print(_parties!.first.id);
      item = RefreshIndicator(
        child: Container(
          padding: EdgeInsets.all(8.w),
          /*
          child: GridView.builder(
            itemCount: _parties!.length,
            controller: _scrollController,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              crossAxisSpacing: 8.w,
              mainAxisSpacing: 8.w,
              childAspectRatio: 0.5.sw / (265 + 16.w)
            ),
            itemBuilder: (BuildContext context, int index){
              return PartyBox(party: _parties![index],);
            },
          )
          */
          child: StaggeredGridView.countBuilder(
            controller: _scrollController,
            crossAxisCount: 4,
            itemCount: (_parties != null) ? _parties!.length : 0,
            itemBuilder: (BuildContext context, int index) {
              return PartyBox(parties: _parties!, index: index,);
            },
            staggeredTileBuilder: (int index) =>
              StaggeredTile.fit(2),
            mainAxisSpacing: 8.w,
            crossAxisSpacing: 8.w,
          )
        ),
        onRefresh: () async {
          _completer = Completer();
          context.read<PartyListingBloc>().add(PartyListFetch());
          return _completer!.future;
        }
      );
    }

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        centerTitle: true,
        title: Text('ปาร์ตี้ทั้งหมด'),
        actions: [
          IconButton(icon: Icon(Icons.add), onPressed: () async {
            final response = await Navigator.pushNamed(context, 'party-form');

            if (response is Party) {
              ScaffoldMessenger.of(context)..hideCurrentSnackBar()..showSnackBar(
                LmsSnackbar().success(context: context, message: 'คุณสร้างปาร์ตี้สำเร็จ')
              );
              context.read<PartyListingBloc>().add(PartyListFetch());
            }
          })
        ],
      ),
      body: item
    );
  }
}