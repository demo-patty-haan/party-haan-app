import 'dart:async';
import 'package:bloc/bloc.dart';

import './MainScreen.dart';

class MainScreenBloc extends Bloc<MainScreenEvent, MainScreenState> {

  MainScreenBloc() : super(MainInitial());

  @override
  Stream<MainScreenState> mapEventToState(MainScreenEvent event) async* {
    if (event is MainScreenChangeTab) {
      yield MainScreenTabChanged(index: event.index);
    } else if (event is MainScreenInit) {
      yield* _mapMainScreenInit(event);
    }
  }

  Stream<MainScreenState> _mapMainScreenInit(MainScreenInit event) async* {

  }
}