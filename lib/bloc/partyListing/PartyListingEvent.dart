import 'package:equatable/equatable.dart';

import '../../models/Party.dart';

abstract class PartyListEvent extends Equatable {
  PartyListEvent();

  @override
  List<Object> get props => [];
}

class PartyListFetch extends PartyListEvent {
  PartyListFetch();
}

class PartyListFetchMore extends PartyListEvent {
  final List<Party> parties;

  PartyListFetchMore({required this.parties});
}