import 'package:equatable/equatable.dart';

abstract class LoginMainState extends Equatable {
  const LoginMainState();

  @override
  List<Object> get props => [];
}

class LoginMainInitial extends LoginMainState {}
class LoginMainLoading extends LoginMainState {}
class LoginMainScreenLoading extends LoginMainState {}
class LoginMainSuccess extends LoginMainState {}
class LoginMainFail extends LoginMainState {}