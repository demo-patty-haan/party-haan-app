import 'dart:async';
import 'package:bloc/bloc.dart';

import './PartyForm.dart';
import '../../repositories/PartyRepository.dart';
import '../../models/Party.dart';

class PartyFormBloc extends Bloc<PartyFormEvent, PartyFormState> {
  final PartyRepository partyRepository;

  PartyFormBloc({required this.partyRepository}): super(PartyFormInitial());

  @override
  Stream<PartyFormState> mapEventToState(PartyFormEvent event) async* {
    if (event is PartyFormSubmit) {
      yield* _mapPartyFormSubmit(event);
    }
  }

  Stream<PartyFormState> _mapPartyFormSubmit(PartyFormSubmit event) async* {
    try {
      yield PartyFormProcessing();
      Party party = await partyRepository.createParty(name: event.name, maxTotalUsers: event.maxTotalUser);
      yield PartyFormSubmited(party: party);
    } catch (e) {
      yield PartyFormFailure(message: e.toString());
    }
  }
}