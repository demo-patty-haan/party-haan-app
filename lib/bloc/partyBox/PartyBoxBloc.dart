import 'dart:async';
import 'package:bloc/bloc.dart';

import './PartyBox.dart';
import '../authenticate/AuthenticationBloc.dart';
import '../../managers/PartyManager.dart';
import '../../models/Party.dart';

class PartyBoxBloc extends Bloc<PartyBoxEvent, PartyBoxState> {
  final AuthenticationBloc authenticationBloc;
  final PartyManager partyManager;

  PartyBoxBloc({required this.authenticationBloc, required this.partyManager}) : super(PartyBoxInitial());

  @override
  Stream<PartyBoxState> mapEventToState(PartyBoxEvent event) async* {
    if (event is JoinParty) {
      yield* _mapJoinParty(event);
    } else if (event is UnJoinParty) {
      yield* _mapUnJoinParty(event);
    }
  }

  Stream<PartyBoxState> _mapJoinParty(JoinParty event) async* {
    try {
      yield PartyBoxProcessing();
      Party party = await partyManager.joinParty(party: event.party,);
      yield PartyBoxLoaded(party: party, message: 'เข้าร่วมสำเร็จ');
    } catch (e) {
      yield PartyBoxFailure(message: e.toString());
    }
  }

  Stream<PartyBoxState> _mapUnJoinParty(UnJoinParty event) async* {
    try {
      yield PartyBoxProcessing();
      Party party = await partyManager.unJoinParty(party: event.party, user: authenticationBloc.sessionUser!);
      yield PartyBoxLoaded(party: party, message: 'ยกเลิกสำเร็จ');
    } catch (e) {
      yield PartyBoxFailure(message: e.toString());
    }
  }
}