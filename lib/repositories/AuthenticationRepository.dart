import 'dart:convert';
import 'dart:io';
import 'package:dio/dio.dart';

import '../data/ApiClient.dart';

class AuthenticationRepository {
  final ApiClient apiClient;

  AuthenticationRepository({required this.apiClient});

  Future<dynamic> loginCheck({required String username, required String password}) async {
    final String path = '/public/v1/login_check';

    dynamic body = {
      'username': username,
      'password': password,
    };

    Response response = await apiClient.post(path, jsonEncode(body), auth: false);

    if (response.statusCode != HttpStatus.created) {
      throw('invalid login');
    }

    if (response.data is String) {
      return jsonDecode(response.data);
    }
    return response.data;
  }
}