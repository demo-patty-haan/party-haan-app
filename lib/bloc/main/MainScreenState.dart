import 'package:equatable/equatable.dart';

abstract class MainScreenState extends Equatable {
  MainScreenState();

  @override
  List<Object> get props => [];
}

class MainInitial extends MainScreenState {}
class MainScreenTabChanged extends MainScreenState {
  final int index;
  MainScreenTabChanged({required this.index});

  @override
  List<Object> get props => [index];
}

class MainBackGroundProcessing extends MainScreenState {}