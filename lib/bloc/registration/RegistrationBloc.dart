import 'dart:async';
import 'package:bloc/bloc.dart';

import './Registration.dart';
import '../../models/User.dart';

import '../../repositories/UserRepository.dart';
import '../../managers/AuthenticationManager.dart';
import '../../bloc/authenticate/AuthenticationBloc.dart';
import '../../bloc/authenticate/AuthenticationEvent.dart';

class RegistrationBloc extends Bloc<RegistrationEvent, RegistrationState> {
  final UserRepository userRepository;
  final AuthenticationManager authenticationManager;
  final AuthenticationBloc authenticationBloc;

  RegistrationBloc({required this.userRepository, required this.authenticationManager, required this.authenticationBloc}): super(RegistrationInitial());

  @override
  Stream<RegistrationState> mapEventToState(RegistrationEvent event) async* {
    if (event is SubmitUserAndPassword) {
      yield* _mapSubmitUserAndPassword(event);
    }
  }

  Stream<RegistrationState> _mapSubmitUserAndPassword(SubmitUserAndPassword event) async* {
    yield RegistrationProcessing();

    try {
      String token = await authenticationManager.createUser(
        username: event.username,
        password: event.password
      );

      authenticationBloc.add(LoggedIn(token: token));

      User user = await userRepository.getCurrentUser();
      yield RegistrationSuccessfull(user: user);

    } catch (e) {
      yield FormFail(
        message: e.toString(),
        violations: null
      );
    }
  }
}