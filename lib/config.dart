import 'package:flutter_secure_storage/flutter_secure_storage.dart';

enum Environment {
  STAGING,
  DEV,
  PRODUCTION,
}

class Config {

  // Singleton
  static final Config _singleton = Config._internal();
  factory Config() {
    return _singleton;
  }
  Config._internal();

  late Environment appEnvironment;
  var app;

  Future<void> setEnvironment(Environment environment, {bool persist = false}) async {
    this.appEnvironment = environment;
    switch (appEnvironment) {
      case Environment.STAGING:
        this.app = {
          'wwwHost': 'party-haan-api.peglowny.com',
          'port': null,
          'httpScheme': 'https',
          'envName': 'integration',
        };
        break;
      case Environment.DEV:
        this.app = {
          'wwwHost': 'localhost',
          'port': '9022',
          'httpScheme': 'http',
          'envName': 'dev'
        };
        break;
      default:
        this.app = {
          'wwwHost': 'party-haan-api.peglowny.com',
          'port': null,
          'httpScheme': 'https',
          'envName': 'production'
        };
    }


    if (persist) {
      FlutterSecureStorage storage = FlutterSecureStorage();
      await storage.write(key: 'env', value: environment.toString());
    }

  }

  Future<void> loadEnv() async {
    FlutterSecureStorage storage = FlutterSecureStorage();
    String? env = await storage.read(key: 'env');
    print('env $env');

    if (env == Environment.PRODUCTION.toString()) {
      appEnvironment = Environment.PRODUCTION;
    } else if (env == Environment.DEV.toString()) {
      appEnvironment = Environment.DEV;
    } else if (env == Environment.STAGING.toString()) {
      appEnvironment = Environment.STAGING;
    } else {
      appEnvironment = Environment.PRODUCTION;
    }

    await setEnvironment(appEnvironment);
  }

  String get baseUrl {
    return this.app['httpScheme'] + '://' + this.app['wwwHost'];
  }

  String get playStoreUrl {
    return 'market://details?id=${this.app['playStoreId']}';
  }

  String get appStoreUrl {
    return 'itms-apps://itunes.apple.com/app/apple-store/id${this.app['appStoreId']}';
  }

  bool get isStaging {
    return appEnvironment == Environment.STAGING;
  }
}