import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import '../repositories/UserRepository.dart';
import '../repositories/AuthenticationRepository.dart';

class AuthenticationManager{
  final FlutterSecureStorage storage;
  final AuthenticationRepository authenticationRepository;
  final UserRepository userRepository;

  AuthenticationManager({
    required this.storage,
    required this.authenticationRepository,
    required this.userRepository,
  });

  Future<String> authenticate({
    required String username,
    required String password
  }) async {
    Map<String, dynamic> json = await authenticationRepository.loginCheck(username: username, password: password);
    String token = json['token'];
    String refreshToken = json['refresh_token'];
    await persistToken(token, refreshToken);
    return token;
  }

  Future<String> createUser({required String username, required String password}) async {
    await userRepository.createUser(username: username, password: password);
    return await authenticate(username: username, password: password);
  }

  Future<String> getToken() async {
    String? token = await storage.read(key: 'token');
    if (token == null) {
      throw Exception();
    }
    return token;
  }

  Future<void> persistToken(String token, String refreshToken) async {
    await storage.write(key: 'token', value: token);
    await storage.write(key: 'refresh_token', value: refreshToken);
  }

  Future<void> deleteToken() async {
    await storage.delete(key: 'token');
    await storage.delete(key: 'refresh_token');
  }

}