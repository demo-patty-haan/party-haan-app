import 'package:equatable/equatable.dart';

import '../../models/Party.dart';

abstract class PartyListState extends Equatable {
  PartyListState();

  @override
  List<Object> get props => [];
}

class PartyListInitial extends PartyListState {}

class PartyListProcessing extends PartyListState {}

class PartyListProcessingLoadmore extends PartyListState {}

class PartyListLoaded extends PartyListState {
  final List<Party> parties;
  final bool hasReachMax;

  PartyListLoaded({required this.parties, required this.hasReachMax});
}

class PartyListFailure extends PartyListState {
  final String message;

  PartyListFailure({required this.message});
}