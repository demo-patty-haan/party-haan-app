import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../bloc/loginMain/LoginMain.dart';
import '../bloc/authenticate/AuthenticationBloc.dart';
import '../managers/AuthenticationManager.dart';
import '../repositories/UserRepository.dart';

import '../widgets/common/DefaultTextInput.dart';
import '../widgets/common/PasswordTextField.dart';
import '../widgets/common/PrimaryButton.dart';

class LoginMainScreen extends StatefulWidget {
  LoginMainScreen({ Key? key }) : super(key: key);

  @override
  _LoginMainScreenState createState() => _LoginMainScreenState();

}

class _LoginMainScreenState extends State<LoginMainScreen> {
  final _formKey = GlobalKey<FormState>();
  String _password = '';
  String _username = '';

  _onLoginButtonPressed(BuildContext context) {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();
      context.read<LoginMainBloc>().add(LoginButtonPressed(username: _username.trim(), password: _password.trim()));
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<LoginMainBloc>(
      create: (BuildContext context) {
        return LoginMainBloc(
          authenticationBloc: context.read<AuthenticationBloc>(),
          authenticationManager: context.read<AuthenticationManager>(),
          userRepository: context.read<UserRepository>()
        );
      },
      child: BlocConsumer<LoginMainBloc, LoginMainState>(
        listener: _mainContentListenter,
        builder: _mainContentBuilder,
      ),
    );
  }

  void _mainContentListenter(BuildContext context, LoginMainState state) async {

  }

  Widget _mainContentBuilder(BuildContext context, LoginMainState state) {


    return Scaffold(
      body: Container(
        width: 1.sw,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.bottomLeft,
            end: Alignment.topRight,
            stops: [0.25, 0.75],
            colors: [
              Color.fromRGBO(225,118,173,1.0),
              Color.fromRGBO(110,198,255,1.0),
            ],
          )
        ),
        alignment: Alignment.center,
        child: SingleChildScrollView(
          padding: EdgeInsets.only(left: 24.w, right: 24.w),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Center(
                  child: Container(
                  width: 0.3.sw,
                  height: 120,
                  decoration: BoxDecoration(
                    color: Colors.black38
                  ),
                  child: Center(
                    child: Text('LOGO', style: Theme.of(context).textTheme.headline4!.copyWith(color: Colors.white),),
                  ),
                ),
                ),
                SizedBox(height: 32,),
                Text('อีเมล', style: Theme.of(context).textTheme.subtitle1,),
                SizedBox(height: 8,),
                DefaultTextInput(
                key: Key('userInput'),
                label:  'อีเมล',
                validator: (value) {
                  if (value == null) {
                    return null;
                  }

                  if (value.isEmpty) {
                    return 'เกิดข้อผิดพลาด';
                  }
                  return null;
                },
                onSaved: (val) {
                  _username = val ?? '';
                },
              ),
              SizedBox(height: 16,),
              Text('รหัสผ่าน', style: Theme.of(context).textTheme.subtitle1,),
              SizedBox(height: 8,),
              PasswordTextField(
                key: Key('passwordInput'),
                onSaved: (val) => _password = val ?? '',
                label: 'รหัสผ่าน',
                validator: (value) {
                  if (value == null) {
                    return null;
                  }

                  if (value.isEmpty) {
                    return 'เกิดข้อผิดพลาด';
                  }
                  return null;
                },
              ),
              SizedBox(height: 40,),
              Center(
                child: SizedBox(
                  width: 0.5.sw,
                  child: PrimaryButton(
                    onPressed: (state is! LoginMainLoading) ? () {
                      _onLoginButtonPressed(context);
                    } : null,
                    text: 'เข้าสู่ระบบ',
                    isProcessing: (state is LoginMainLoading),
                  ),
                ),
              ),
              SizedBox(height: 32,),
              Center(
                child: SizedBox(
                  width: 0.5.sw,
                  child: PrimaryButton(
                    onPressed: () async {
                      await Navigator.pushNamed(context, 'registration');
                    },
                    text: 'สร้างบัญชีผู้ใช้',
                    isProcessing: false,
                  ),
                ),
              ),
            ]),
          )
        ),
      ),
    );
  }
}