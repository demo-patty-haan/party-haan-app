import 'dart:async';
import 'package:bloc/bloc.dart';

import './PartyListing.dart';
import '../../repositories/PartyRepository.dart';
import '../../models/Party.dart';

class PartyListingBloc extends Bloc<PartyListEvent, PartyListState> {
  final PartyRepository partyRepository;
  final int limit = 30;

  PartyListingBloc({required this.partyRepository}) : super(PartyListInitial());

  @override
  Stream<PartyListState> mapEventToState(PartyListEvent event) async* {
    if (event is PartyListFetch) {
      yield* _mapPartyListFetch(event);
    } else if (event is PartyListFetchMore) {
      yield* _mapPartyListFetchMore(event);
    }
  }

  Stream<PartyListState> _mapPartyListFetch(PartyListFetch event) async* {

    try {
      yield PartyListProcessing();
      List<Party> parties = await partyRepository.getList(limit: limit);
      yield PartyListLoaded(parties: parties, hasReachMax: (parties.length < limit));
    } catch (e) {
      yield PartyListFailure(message: e.toString());
    }
  }

  Stream<PartyListState> _mapPartyListFetchMore(PartyListFetchMore event) async* {
    try {
      yield PartyListProcessingLoadmore();
      List<Party> parties = await partyRepository.getList(limit: limit, page: (event.parties.length/limit).floor() + 1);
      yield PartyListLoaded(parties: event.parties + parties, hasReachMax: (parties.length < limit));
    } catch (e) {
      yield PartyListFailure(message: e.toString());
    }
  }
}