import '../repositories/PartyRepository.dart';
import '../repositories/PartyUserRepository.dart';
import '../models/Party.dart';
import '../models/User.dart';
import '../models/PartyUser.dart';

class PartyManager{
  final PartyRepository partyRepository;
  final PartyUserRepository partyUserRepository;

  PartyManager({required this.partyRepository, required this.partyUserRepository});

  Future<Party> joinParty({required Party party}) async {
    await partyUserRepository.createPartyUser(partyId: party.id!);
    return await partyRepository.getFromId(id: party.id!);
  }

  Future<Party> unJoinParty({required Party party, required User user}) async {
    List<PartyUser> partyUsers = await partyUserRepository.getList(partyId: party.id!, userId: user.id!);

    PartyUser partyUser = partyUsers.first;

    await partyUserRepository.deletePartyUser(id: partyUser.id!);
    return await partyRepository.getFromId(id: party.id!);
  }
}