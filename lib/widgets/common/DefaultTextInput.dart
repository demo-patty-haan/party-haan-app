import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class DefaultTextInput extends StatelessWidget {
  final String label;
  final int minLines;
  final int maxLines;
  final TextEditingController? textEditingController;
  final TextInputType? textInputType;
  final String? Function(String?)? validator;
  final void Function(String?)? onSaved;
  final void Function(String)? onChanged;
  final List<TextInputFormatter>? inputFormatters;
  final bool enabled;

  DefaultTextInput({Key? key, required this.label, this.minLines = 1, this.maxLines = 1, this.textEditingController, this.validator, this.onSaved, this.onChanged, this.inputFormatters, this.enabled = true, this.textInputType});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      autocorrect: false,
      minLines: minLines,
      maxLines: maxLines,
      keyboardType: textInputType,
      inputFormatters: inputFormatters,
      controller: textEditingController,
      enabled: enabled,
      decoration: InputDecoration(
        filled: true,
        fillColor: enabled ? Colors.white : Colors.grey[100],
        labelText: label,
        floatingLabelBehavior: FloatingLabelBehavior.never,
        alignLabelWithHint: true,
        border: InputBorder.none,
        contentPadding: const EdgeInsets.only(left: 14.0, bottom: 20.0, top: 10.0),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Color(0XFFCBCBCB)),
          borderRadius: BorderRadius.circular(10),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Color(0XFFCBCBCB)),
          borderRadius: BorderRadius.circular(10),
        ),
        errorBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Theme.of(context).errorColor),
          borderRadius: BorderRadius.circular(10),
        ),
        disabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Theme.of(context).disabledColor),
          borderRadius: BorderRadius.circular(10),
        ),
        focusedErrorBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Theme.of(context).errorColor),
          borderRadius: BorderRadius.circular(10),
        ),
      ),
      validator: validator,
      onSaved: onSaved,
      onChanged: onChanged,
    );
  }
}