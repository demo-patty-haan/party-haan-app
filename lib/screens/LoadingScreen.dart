import 'package:flutter/material.dart';
import 'package:loading_animations/loading_animations.dart';

class LoadingScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.bottomLeft,
            end: Alignment.topRight,
            stops: [0.25, 0.75],
            colors: [
              Color.fromRGBO(225,118,173,1.0),
              Color.fromRGBO(110,198,255,1.0),
            ],
          )
        ),
        child: Center(
          child: Center(
            child: LoadingBouncingGrid.square(
                size: 100,
                backgroundColor: Colors.white
              )
          ),
        ),
      )
    );
  }
}