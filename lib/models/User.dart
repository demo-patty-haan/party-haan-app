import 'package:equatable/equatable.dart';

class User extends Equatable {
  final int? id;
  final DateTime? created;
  final DateTime? updated;
  final String? username;

  @override
  List<Object> get props => [id!];

  User({
    this.id,
    this.created,
    this.updated,
    this.username
  });

  User copyWith({
    int? id,
    String? username,
  }) {
    return User(
      id: id ?? this.id,
      username: username ?? this.username,
      created: created,
      updated: updated
    );
  }

  static User fromJson(Map<String, dynamic> json) {
    return User(
      id: json['id'],
      username: json['username'],
      created: (json['created'] != null) ? DateTime.parse(json['created']) : null,
      updated: (json['updated'] != null) ? DateTime.parse(json['updated']) : null,
    );
  }
}