import 'package:equatable/equatable.dart';

abstract class MainScreenEvent extends Equatable {
  MainScreenEvent();

  @override
  List<Object> get props => [];
}

class MainScreenChangeTab extends MainScreenEvent {
  final int index;
  MainScreenChangeTab({required this.index});
}

class MainScreenInit extends MainScreenEvent {}

