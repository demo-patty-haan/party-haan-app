import 'dart:convert';
import 'package:dio/dio.dart';

import '../data/ApiClient.dart';
import '../models/Party.dart';

class PartyRepository {
  final ApiClient apiClient;

  PartyRepository({required this.apiClient});

  Map<String, dynamic> decodeResponse(Response response) {
    if (response.data is String) {
      return jsonDecode(response.data);
    }
    return response.data;
  }

  Future<List<Party>> getList({int? limit = 30, int? page = 1}) async {
    Map<String, String> queryParameters = {
      'limit': limit.toString(),
      'page': page.toString()
    };

    final String path = '/auth/v1/parties';

    Response response = await apiClient.get(path, queryParameters: queryParameters);

    if (response.statusCode! >= 400) {
      throw Exception('Bad request');
    }

    List<Party> parties = [];
    Map<String, dynamic> jsonObject = decodeResponse(response);

    jsonObject['parties'].forEach((element) =>
      parties.add(Party.fromJson(element)));

    return parties;
  }


  Future<Party> getFromId({required int id}) async {
    final String path = '/auth/v1/party/$id';

    Response response = await apiClient.get(path);

    if (response.statusCode! >= 400) {
      throw Exception('Bad request');
    }

    Map<String, dynamic> json = decodeResponse(response);
    return Party.fromJson(json['party']);
  }

  Future<Party> createParty({required String name, required int maxTotalUsers}) async {
    final String path = '/auth/v1/party';

    Map<String, dynamic> data = {
      'name': name,
      'max_total_users': maxTotalUsers
    };

    Response response = await apiClient.post(path, data);
    Map<String, dynamic> json = decodeResponse(response);

    if (response.statusCode! >= 400) {
      throw 'Something went wrong';
    }

    return Party.fromJson(json['party']);
  }

}