import 'package:equatable/equatable.dart';

import '../../models/User.dart';

abstract class RegistrationState extends Equatable {
  RegistrationState();

  @override
  List<Object> get props => [];
}

class RegistrationInitial extends RegistrationState {}

class RegistrationProcessing extends RegistrationState {}

class RegistrationSuccessfull extends RegistrationState {
  final User user;

  RegistrationSuccessfull({required this.user});
}

class FormFail extends RegistrationState {
  final String? message;
  final List<Map<String, String>>? violations;
  FormFail({required this.message, required this.violations});

}