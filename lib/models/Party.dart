import 'package:equatable/equatable.dart';

class Party extends Equatable {
  final int? id;
  final DateTime? created;
  final DateTime? updated;
  final String? name;
  final int? maxTotalUsers;
  final int? totalUsers;
  final bool? isJoined;

  @override
  List<Object> get props => [id!];

  Party({
    this.id,
    this.created,
    this.updated,
    this.name,
    this.maxTotalUsers,
    this.totalUsers,
    this.isJoined,
  });

  static Party fromJson(Map<String, dynamic> json) {

    return Party(
      id: json['id'],
      name: json['name'],
      maxTotalUsers: json['max_total_users'],
      totalUsers: json['total_users'],
      isJoined: json['is_joined'],
      created: (json['created'] != null) ? DateTime.parse(json['created']) : null,
      updated: (json['updated'] != null) ? DateTime.parse(json['updated']) : null,
    );
  }

  bool get isFull {
    if (totalUsers == null || maxTotalUsers == null) {
      return false;
    }
    return totalUsers! >= maxTotalUsers!;
  }
}