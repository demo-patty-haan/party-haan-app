import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../bloc/partyForm/PartyForm.dart';
import '../repositories/PartyRepository.dart';

import '../widgets/common/DefaultTextInput.dart';
import '../widgets/common/PrimaryButton.dart';
import '../widgets/common/LmsSnackbar.dart';

class PartyFormScreen extends StatefulWidget {
  PartyFormScreen({Key? key}) : super(key: key);

  @override
  State<PartyFormScreen> createState() => _PartyFormScreenState();
}

class _PartyFormScreenState extends State<PartyFormScreen> {
  final _formKey = GlobalKey<FormState>();
  late TextEditingController nameController;
  late TextEditingController maxTotalUsersController;

  @override
  void initState() {
    super.initState();

    nameController = TextEditingController();
    maxTotalUsersController = TextEditingController();

    maxTotalUsersController.text = '5';
  }

  @override
  void dispose() {
    nameController.dispose();
    maxTotalUsersController.dispose();

    super.dispose();
  }

  void dismissKeyboard(BuildContext context) {
    FocusManager.instance.primaryFocus!.unfocus();
  }

  @override
  Widget build(BuildContext context) {

    return BlocProvider<PartyFormBloc>(
      create: (BuildContext context) {
        return PartyFormBloc(partyRepository: context.read<PartyRepository>());
      },
      child: BlocConsumer<PartyFormBloc, PartyFormState>(
        listener: _mainContentListenter,
        builder: _mainContentBuilder
      ),
    );
  }

  void _mainContentListenter(BuildContext context, PartyFormState state) async {
    if (state is PartyFormSubmited) {
      Navigator.pop(context, state.party);
    } else if (state is PartyFormFailure) {
      ScaffoldMessenger.of(context)..hideCurrentSnackBar()..showSnackBar(
        LmsSnackbar().error(context: context, message: state.message)
      );
    }
  }

  Widget _mainContentBuilder(BuildContext context, PartyFormState state) {
    return _scaffold(context, state);
  }

  Widget _scaffold(BuildContext context, PartyFormState state) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        centerTitle: true,
        title: Text('สร้างปาร์ตี้'),
      ),
      body: Container(
        width: 1.sw,
        alignment: Alignment.center,
        child: SingleChildScrollView(
          padding: EdgeInsets.only(left: 24.w, right: 24.w),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('ชื่อปาร์ตี้', style: Theme.of(context).textTheme.subtitle1,),
                SizedBox(height: 8,),
                DefaultTextInput(
                  label:  'ชื่อปาร์ตี้',
                  validator: (value) {
                    if (value == null) {
                      return null;
                    }
                    if (value.isEmpty) {
                      return 'ไม่อนุญาตค่าว่าง';
                    }
                    return null;
                  },
                  textEditingController: nameController,
                ),
                SizedBox(height: 16,),
                Text('จำนวนคนที่ขาด', style: Theme.of(context).textTheme.subtitle1,),
                SizedBox(height: 8,),
                DefaultTextInput(
                  label:  'จำนวนคนที่ขาด',
                  validator: (value) {
                    if (value == null) {
                      return null;
                    }
                    if (value.isEmpty) {
                      return 'ไม่อนุญาตค่าว่าง';
                    }
                    if (int.parse(value) > 99) {
                      return 'จำนวนคนห้ามเกิน 99';
                    }
                    return null;
                  },
                  inputFormatters: <TextInputFormatter>[
                    FilteringTextInputFormatter.digitsOnly
                  ],
                  textInputType: TextInputType.number,
                  textEditingController: maxTotalUsersController,
                ),
                SizedBox(height: 40,),
                Center(
                  child: SizedBox(
                    width: 0.5.sw,
                    child: PrimaryButton(
                      onPressed: (state is! PartyFormProcessing) ? () {
                        if (_formKey.currentState!.validate()) {
                          _formKey.currentState!.save();

                          context.read<PartyFormBloc>().add(
                            PartyFormSubmit(
                              name: nameController.text,
                              maxTotalUser: int.parse(maxTotalUsersController.text)
                            )
                          );
                        }
                      } : null,
                      text: 'สร้างปาร์ตี้',
                      isProcessing: (state is PartyFormProcessing || state is PartyFormSubmited),
                    ),
                  ),
                ),
              ]
            )
          )
        )
      )
    );
  }
}