import 'dart:convert';
import 'package:dio/dio.dart';

import '../data/ApiClient.dart';
import '../models/PartyUser.dart';

class PartyUserRepository {
  final ApiClient apiClient;

  PartyUserRepository({required this.apiClient});

  Map<String, dynamic> decodeResponse(Response response) {
    if (response.data is String) {
      return jsonDecode(response.data);
    }
    return response.data;
  }

  Future<List<PartyUser>> getList({required int partyId, required int userId}) async {
    final String path = '/auth/v1/party_users';

    Map<String, String> queryParameters = {
      'party_id': partyId.toString(),
      'user_id': userId.toString()
    };

    Response response = await apiClient.get(path, queryParameters: queryParameters);

    if (response.statusCode! >= 400) {
      throw Exception('Bad request');
    }

    List<PartyUser> partyUsers = [];
    Map<String, dynamic> jsonObject = decodeResponse(response);

    jsonObject['party_users'].forEach((element) =>
      partyUsers.add(PartyUser.fromJson(element)));

    return partyUsers;
  }

  Future<PartyUser> createPartyUser({required int partyId}) async {
    final String path = '/auth/v1/party_user';

    Map<String, dynamic> data = {
      'party_id': partyId,
    };
    Response response = await apiClient.post(path, data);
    Map<String, dynamic> json = decodeResponse(response);

    if (response.statusCode! >= 400) {
      throw 'Something went wrong';
    }

    return PartyUser.fromJson(json['party_user']);
  }

  Future<void> deletePartyUser({required int id}) async {
    final String path = '/auth/v1/party_user/$id';

    Response response = await apiClient.delete(path);

    if (response.statusCode! >= 400) {
      throw 'Something went wrong';
    }
  }

}