import 'package:flutter/material.dart';

class ConfirmationAlertDialog extends StatelessWidget {
  static const String confirm = 'confirm';

  final String questionMessage;
  final String confirmMessage;

  ConfirmationAlertDialog({Key? key, required this.questionMessage, required this.confirmMessage}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      contentPadding: EdgeInsets.all(0),
      title: Icon(Icons.info, size: 40,),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(15.0))
      ),
      content: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(height: 20,),
            Text(questionMessage, style: Theme.of(context).textTheme.headline5, textAlign: TextAlign.center,),
            SizedBox(height: 20,),
            Container(
              padding: EdgeInsets.only(left: 20, right: 20),
              child: Text(confirmMessage, style: Theme.of(context).textTheme.bodyText2, textAlign: TextAlign.center,),
            ),
            SizedBox(height: 40,),
            Container(
              decoration: BoxDecoration(
                border: Border(
                  top: BorderSide(width: 1.0, color: Color(0Xffe1e6f6)),
                )
              ),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      decoration: BoxDecoration(
                        border: Border(
                          right: BorderSide(width: 1.0, color: Color(0Xffe1e6f6)),
                        )
                      ),
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          elevation: 0,
                          primary: Color.fromARGB(0, 0, 0, 0),
                        ),
                        key: Key('ConfirmAlertDialogCancelBtn'),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: Text('ยกเลิก', style: Theme.of(context).textTheme.headline6,),
                      ),
                    )
                  ),
                  Expanded(
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Color.fromARGB(0, 0, 0, 0),
                        elevation: 0,
                      ),
                      key: Key('ConfirmAlertDialogCancelBtn'),
                      onPressed: () {
                        Navigator.pop(context, confirm);
                      },
                      child: Text('ยืนยัน', style: Theme.of(context).textTheme.headline6),
                    ),
                  ),
                ],
              )
            )

          ],
        ),
      ),
    );
  }
}