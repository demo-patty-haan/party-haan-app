import 'package:equatable/equatable.dart';

import '../../models/Party.dart';

abstract class PartyFormState extends Equatable {
  PartyFormState();

  @override
  List<Object> get props => [];
}

class PartyFormInitial extends PartyFormState {}

class PartyFormProcessing extends PartyFormState {}

class PartyFormSubmited extends PartyFormState {
  final Party party;

  PartyFormSubmited({required this.party});
}

class PartyFormFailure extends PartyFormState {
  final String message;

  PartyFormFailure({required this.message});
}