import 'package:equatable/equatable.dart';

import '../../models/Party.dart';

abstract class PartyBoxEvent extends Equatable {
  PartyBoxEvent();

  @override
  List<Object> get props => [];
}

class JoinParty extends PartyBoxEvent {
  final Party party;

  JoinParty({required this.party});
}

class UnJoinParty extends PartyBoxEvent {
  final Party party;

  UnJoinParty({required this.party});
}