import 'dart:async';
import 'dart:io';
import 'package:dio/dio.dart' as Dio;
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import '../config.dart';

class ApiClient {
  static String userAgent = '';
  static const Duration defaultDuration = Duration(seconds: 10);

  final FlutterSecureStorage storage;
  ApiClient({required this.storage});


  Future<Map<String, String>> getHeaders() async {
    Map<String, String> headers = {
      HttpHeaders.acceptHeader: 'application/json',
      HttpHeaders.contentTypeHeader: 'application/json',
    };

    return headers;
  }

  Future<String?> getToken() async {
    return await storage.read(key: 'token');
  }

  Future<Dio.BaseOptions> getBaseOptions({bool auth = false}) async {

    Map<String, String> headers = {
      HttpHeaders.acceptHeader: 'application/ld+json',
    };

    if (auth) {
      String? token = await storage.read(key: 'token');
      if (token != null) {
        headers[HttpHeaders.authorizationHeader] = 'Bearer ' + token;
      }
    }

    return Dio.BaseOptions(
      connectTimeout: 5000,
      receiveTimeout: 3000,
      baseUrl: Uri(scheme: Config().app['httpScheme'], host: Config().app['wwwHost'],port: Config().app['port']).toString(),
      headers: headers
    );
  }

  Future<Dio.Response> get(String path, {
    Map<String, String>? queryParameters,
    Duration duration = defaultDuration,
    bool auth = true
  }) async {
    print('get: $path');
    print('query: $queryParameters');
    Dio.Response response = await Dio.Dio(await getBaseOptions(auth: auth)).get(path,
      queryParameters: queryParameters,
      options: Dio.Options(
        validateStatus: (status) {
          return status! <= 500;
        }
      )
    );
    print(response.data);
    return response;
  }

  Future<Dio.Response> post(String path, dynamic body, {
    Duration duration = defaultDuration,
    Map<String, String>? queryParameters,
    bool auth = true
  }) async {
    print('post: $path');
    print(body);
    Dio.Response response = await Dio.Dio(await getBaseOptions(auth: auth)).post(path,
      queryParameters: queryParameters,
      data: body,
      options: Dio.Options(
        validateStatus: (status) {
          return status! <= 500;
        }
      )
    );
    print(response.data);
    // dio2curl(response.request);
    //print(dio2curl(response.request));
    return response;
  }

  String dio2curl(Dio.RequestOptions requestOption) {
  var curl = '';



  // Add PATH + REQUEST_METHOD
  curl += 'curl --request ${requestOption.method} \'${requestOption.path}\'';

  // Include headers
  for(var key in requestOption.headers.keys) {
    print(' -H \'$key: ${requestOption.headers[key]}\'');
    curl += ' -H \'$key: ${requestOption.headers[key]}\'';
  }

  // Include data if there is data
  if(requestOption.data != null) {
    print(' --data-binary \'${requestOption.data}\'');
    curl += ' --data-binary \'${requestOption.data}\'';
  }

  curl += ' --insecure'; //bypass https verification


  return curl;
}


  Future<Dio.Response> patch(String path, dynamic body, {
    Duration duration = defaultDuration,
    Map<String, String>? queryParameters,
    bool auth = true
  }) async {
    print('patch: $path');
    print(body);
    Dio.Response response = await Dio.Dio(await getBaseOptions(auth: auth)).patch(path,
      queryParameters: queryParameters,
      data: body,
      options: Dio.Options(
        validateStatus: (status) {
          return status! <= 500;
        }
      )
    );
    print(response.data);
    return response;
  }

  Future<Dio.Response> delete(String path, {
    Duration duration = defaultDuration,
    Map<String, String>? queryParameters,
    bool auth = true
  }) async {
    print('patch: $path');
    Dio.Response response = await Dio.Dio(await getBaseOptions(auth: auth)).delete(path,
      queryParameters: queryParameters,
      options: Dio.Options(
        validateStatus: (status) {
          return status! <= 500;
        }
      )
    );
    print(response.data);
    return response;
  }


  Future<Dio.Response> getExternal(String path, {
    Map<String, String>? queryParameters,
    Duration duration = defaultDuration,
  }) async {
    print('get: $path');
    print('query: $queryParameters');
    Dio.Response response = await Dio.Dio().get(path,
      queryParameters: queryParameters,
      options: Dio.Options(
        validateStatus: (status) {
          return status! <= 500;
        }
      )
    );
    print(response.data);
    return response;
  }
}