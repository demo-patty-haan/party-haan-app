import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:package_info/package_info.dart';

import '../bloc/authenticate/AuthenticationBloc.dart';
import '../bloc/authenticate/AuthenticationEvent.dart';
import '../models/User.dart';

class ProfileScreen extends StatefulWidget {
  ProfileScreen({Key? key}) : super(key: key);

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  String _version = '';
  late User _user;
  @override
  void initState() {
    super.initState();

    PackageInfo.fromPlatform().then((PackageInfo value){
      setState(() {
        _version = 'Version ${value.version}';
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    _user = context.read<AuthenticationBloc>().sessionUser!;

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('โปรไฟล์'),
      ),
      body: SafeArea(
        child: CustomScrollView(
          slivers: <Widget>[
            SliverToBoxAdapter(
              child: SizedBox(height: 30,)
            ),
            SliverToBoxAdapter(
              child: Center(
                child: Container(
                  height: 150,
                  width: 0.5.sw,
                  color: Colors.black38,
                  child: Center(child: Text('Avatar', style: Theme.of(context).textTheme.headline4,),),
                )
              )
            ),
            SliverToBoxAdapter(
              child: SizedBox(height: 40,)
            ),
            SliverToBoxAdapter(
              child: Center(
                child: Text(_user.username ?? '', style: Theme.of(context).textTheme.subtitle1,),
              )
            ),
            SliverToBoxAdapter(
              child: SizedBox(height: 40,)
            ),
            SliverToBoxAdapter(
              child: Center(
                child: InkWell(
                  onTap: () {
                    BlocProvider.of<AuthenticationBloc>(context).add(LoggedOut());
                  },
                  child: Text('ออกจากระบบ', style: Theme.of(context).textTheme.subtitle1!.copyWith(decoration: TextDecoration.underline)),
                ),
              ),
            ),
            SliverFillRemaining(
              hasScrollBody: false,
              // fillOverscroll: true, // Set true to change overscroll behavior. Purely preference.
              child: Align(
                alignment: Alignment.bottomLeft,
                child: Container(
                  padding: EdgeInsets.only(bottom: 7, top: 7, left: 14.w),
                  child: Text(_version, style: Theme.of(context).textTheme.bodyText2!.copyWith(color: Colors.black26)),
                )
              ),
            )
          ]
        )
      )
    );
  }
}