import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../bloc/main/MainScreen.dart';
import './PartyListingScreen.dart';
import './ProfileScreen.dart';

class MainScreen extends StatefulWidget {
  MainScreen({Key? key}) : super(key: key);

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int _selectedIndex = 0;

  void _onItemTapped(BuildContext context, int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<MainScreenBloc>(
      create: (BuildContext context) {
        return MainScreenBloc()..add(MainScreenInit());
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        body: BlocConsumer<MainScreenBloc, MainScreenState>(
          listener: _mainContentListenter,
          builder: _mainContentBuilder
        ),
        bottomNavigationBar: BlocBuilder<MainScreenBloc, MainScreenState>(
          builder: (BuildContext context, MainScreenState state) {
            return BottomNavigationBar(
              backgroundColor: Colors.white,
              elevation: 10,
              type: BottomNavigationBarType.fixed,
              key: Key('mainBottomNavigation'),
              items: getBottomMenu(),
              currentIndex: _selectedIndex,
              onTap: (index) {
                _onItemTapped(context, index);
              },
            );
          }
        )
      )
    );
  }

  List<BottomNavigationBarItem> getBottomMenu() {
    List<BottomNavigationBarItem> items = [
      BottomNavigationBarItem(
        icon: Icon(Icons.home),
        label: 'Home',
      ),
      BottomNavigationBarItem(
        icon: Icon(Icons.person_pin_circle),
        label: 'Profile'
      ),
    ];
    return items;
  }

  void _mainContentListenter(BuildContext context, MainScreenState state) async {
  }

  Widget _mainContentBuilder(BuildContext context, MainScreenState state) {
    return _body(context);
  }

  Widget _body(BuildContext context) {
    if (_selectedIndex == 0) {
      return PartyListingScreen();
    } else if (_selectedIndex == 1) {
      return ProfileScreen();
    }

    return Container();
  }
}