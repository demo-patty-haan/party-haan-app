import 'package:equatable/equatable.dart';

import 'Party.dart';
import 'User.dart';

class PartyUser extends Equatable {
  final int? id;
  final DateTime? created;
  final DateTime? updated;
  final Party? party;
  final User? user;

  @override
  List<Object> get props => [id!];

  PartyUser({
    this.id,
    this.created,
    this.updated,
    this.party,
    this.user
  });


  static PartyUser fromJson(Map<String, dynamic> json) {

    return PartyUser(
      id: json['id'],
      created: (json['created'] != null) ? DateTime.parse(json['created']) : null,
      updated: (json['updated'] != null) ? DateTime.parse(json['updated']) : null,
      party: Party.fromJson(json['party']),
      user: User.fromJson(json['user'])
    );
  }
}